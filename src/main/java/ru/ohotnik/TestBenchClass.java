package ru.ohotnik;

import ru.ohotnik.laboratory.api.Benchmark;

/**
 * @author aleksander okhonchenko <strunasa@mail.ru>
 * @since 18.06.2017.
 */
public class TestBenchClass {

  @Benchmark
  public void method() {
    int result = 0;
    for (int i = 0; i < 100; i++) {
      result += i;
    }
    System.out.println(result);
  }

}
